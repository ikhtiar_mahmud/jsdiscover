let screenWidth   = window.screen.width;
let screenHeight  = window.screen.height;
let screenAvailWidth  = window.screen.availWidth;
let screenAvailHeight = window.screen.availHeight;
let screenColorDepth  = window.screen.colorDepth;
let screenPixelDepth  = window.screen.pixelDepth;

document.getElementById("swidth").innerHTML  = "Screen Width is : " + screenWidth; 
document.getElementById("sheight").innerHTML  = "Screen Height is : " + screenHeight; 
document.getElementById("savailWidth").innerHTML  = "Screen Avail Width is : " + screenAvailWidth; 
document.getElementById("savailHeight").innerHTML = "Screen Avail Height is : " + screenAvailHeight; 
document.getElementById("scolorDepth").innerHTML  = "Screen Color Depth is : " + screenColorDepth; 
document.getElementById("spixelDepth").innerHTML  = "Screen Pixel Depth is : " + screenPixelDepth; 